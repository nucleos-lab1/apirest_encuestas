package com.encuestas.application.test;

public class ClienteTests {
	
	private Long idCliente;
	
	private String identificacion;
	
	private String nombres;

	

	public ClienteTests(Long idCliente, String identificacion, String nombres) {
		super();
		this.idCliente = idCliente;
		this.identificacion = identificacion;
		this.nombres = nombres;
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	
}
