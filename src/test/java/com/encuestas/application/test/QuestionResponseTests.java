package com.encuestas.application.test;

public class QuestionResponseTests {

	private Long id;
	private QuestionTests question;
	private String respuesta;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public QuestionTests getQuestion() {
		return question;
	}
	public void setQuestion(QuestionTests question) {
		this.question = question;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	
}
