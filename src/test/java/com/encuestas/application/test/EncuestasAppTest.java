package com.encuestas.application.test;

import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class EncuestasAppTest {

	@Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;
    
    @Test
    public void listadoPreguntasEncuestas() throws Exception {
    	
    	String expectPregunta = "¿El producto cumplía con lo especificado en la web?";
    	
    	mvc.perform(MockMvcRequestBuilders
                .get("/encuesta")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", isA(ArrayList.class)))
                .andExpect(jsonPath("$[0].idQuestion").exists())
                .andExpect(jsonPath("$[0].pregunta").exists())
                .andExpect(jsonPath("$[0].idQuestion", is(1)))
                .andExpect(jsonPath("$[0].pregunta", is(expectPregunta)));
    	
    }
    
    @Test
    public void crearEncuesta() throws Exception {
    	
    	ClienteTests clienteTests = new ClienteTests(1L, "1065639588", "Amilkar Hernandez");
    	QuestionTests questionTests = new QuestionTests();
    	questionTests.setIdQuestion(1L);
    	QuestionTests questionTests1 = new QuestionTests();
    	questionTests1.setIdQuestion(2L);
    	
    	QuestionResponseTests responseTests = new QuestionResponseTests();
    	responseTests.setQuestion(questionTests);
    	responseTests.setRespuesta("SI");
    	QuestionResponseTests responseTests1 = new QuestionResponseTests();
    	responseTests1.setQuestion(questionTests1);
    	responseTests1.setRespuesta("EXCELENTE");
    	
    	List<QuestionResponseTests> listResponse = new ArrayList<>();
    	
    	listResponse.add(responseTests);
    	listResponse.add(responseTests1);
    	
    	mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new EncuestaTests(null, clienteTests, listResponse, null))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.encuesta").exists())
                .andExpect(jsonPath("$.encuesta.idEncuesta").exists())
                .andExpect(jsonPath("$.encuesta.cliente").exists())
                .andExpect(jsonPath("$.encuesta.questionsResponse").exists())
                .andExpect(jsonPath("$.encuesta.questionsResponse", isA(ArrayList.class)))
                .andExpect(jsonPath("$.mensaje", is("Se Guardaron las preguntas Muchas Gracias por sus Respuestas!")))
                .andReturn();
    }
    
    @Test
    public void validacionExistenciaClienteCrearEncuesta() throws Exception {
    	QuestionTests questionTests = new QuestionTests();
    	questionTests.setIdQuestion(1L);
    	QuestionTests questionTests1 = new QuestionTests();
    	questionTests1.setIdQuestion(2L);
    	
    	QuestionResponseTests responseTests = new QuestionResponseTests();
    	responseTests.setQuestion(questionTests);
    	responseTests.setRespuesta("EXCELENTE");
    	QuestionResponseTests responseTests1 = new QuestionResponseTests();
    	responseTests1.setQuestion(questionTests1);
    	responseTests1.setRespuesta("SI");
    	
    	List<QuestionResponseTests> listResponse = new ArrayList<>();
    	
    	listResponse.add(responseTests);
    	listResponse.add(responseTests1);
    	
    	List<String> expected = new ArrayList<>();
    	expected.add("El cliente es Requerido");
    	
    	mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new EncuestaTests(null, null, listResponse, null))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", isA(ArrayList.class)))
                .andExpect(jsonPath("$.errors", is(expected)))
                .andReturn();
    }
    
    @Test
    public void validacionExistenciaPreguntasCrearEncuesta() throws Exception {
    	ClienteTests clienteTests = new ClienteTests(1L, "1065639588", "Amilkar Hernandez");
    	
    	List<String> expected = new ArrayList<>();
    	expected.add("Las preguntas son Requeridas");
    	
    	mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new EncuestaTests(null, clienteTests, null, null))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors", isA(ArrayList.class)))
                .andExpect(jsonPath("$.errors", is(expected)))
                .andReturn();
    }
    
    @Test
    public void validacionRespuestasCorrectasenEncuestaSeleccionMultiple() throws Exception {
    	
    	ClienteTests clienteTests = new ClienteTests(1L, "1065639588", "Amilkar Hernandez");
    	QuestionTests questionTests1 = new QuestionTests();
    	questionTests1.setIdQuestion(1L);
    	QuestionTests questionTests2 = new QuestionTests();
    	questionTests2.setIdQuestion(2L);
    	QuestionTests questionTests3 = new QuestionTests();
    	questionTests3.setIdQuestion(3L);
    	
    	QuestionResponseTests responseTests1 = new QuestionResponseTests();
    	responseTests1.setQuestion(questionTests1);
    	responseTests1.setRespuesta("NOSE");
    	QuestionResponseTests responseTests2 = new QuestionResponseTests();
    	responseTests2.setQuestion(questionTests1);
    	responseTests2.setRespuesta("EXCELENTE");
    	QuestionResponseTests responseTests3 = new QuestionResponseTests();
    	responseTests3.setQuestion(questionTests3);
    	responseTests3.setRespuesta("SI");
    	
    	List<QuestionResponseTests> listResponse = new ArrayList<>();
    	
    	listResponse.add(responseTests1);
    	listResponse.add(responseTests2);
    	listResponse.add(responseTests3);
    	
    	String expected = "Ocurrio un error al guardar la encuesta!";
    	
    	mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new EncuestaTests(null, clienteTests, listResponse, null))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.preguntasConErrores", isA(ArrayList.class)))
                .andExpect(jsonPath("$.mensaje", is(expected)))
                .andReturn();
    }
    
    @Test
    public void validacionRespuestasCorrectasenEncuestaPreguntaAbierta() throws Exception {
    	
    	ClienteTests clienteTests = new ClienteTests(1L, "1065639588", "Amilkar Hernandez");
    	QuestionTests questionTests4 = new QuestionTests();
    	questionTests4.setIdQuestion(4L);
    	
    	QuestionResponseTests responseTests4 = new QuestionResponseTests();
    	responseTests4.setQuestion(questionTests4);
    	responseTests4.setRespuesta("");
    	
    	List<QuestionResponseTests> listResponse = new ArrayList<>();
    	
    	listResponse.add(responseTests4);
    	
    	String expected = "Ocurrio un error al guardar la encuesta!";
    	
    	mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new EncuestaTests(null, clienteTests, listResponse, null))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.preguntasConErrores", isA(ArrayList.class)))
                .andExpect(jsonPath("$.mensaje", is(expected)))
                .andReturn();
    }
}
