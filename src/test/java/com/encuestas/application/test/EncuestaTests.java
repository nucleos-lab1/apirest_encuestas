package com.encuestas.application.test;

import java.util.Date;
import java.util.List;

public class EncuestaTests {

	private Long idEncuesta;
	private ClienteTests cliente;
	private List<QuestionResponseTests> questionsResponse;
	private Date fechaRegistro;
	
	public EncuestaTests(){
		super();
	}
	
	public EncuestaTests(Long idEncuesta, ClienteTests cliente, List<QuestionResponseTests> questionsResponse,
			Date fechaRegistro) {
		super();
		this.idEncuesta = idEncuesta;
		this.cliente = cliente;
		this.questionsResponse = questionsResponse;
		this.fechaRegistro = fechaRegistro;
	}
	
	public Long getIdEncuesta() {
		return idEncuesta;
	}
	public void setIdEncuesta(Long idEncuesta) {
		this.idEncuesta = idEncuesta;
	}
	public ClienteTests getCliente() {
		return cliente;
	}
	public void setCliente(ClienteTests cliente) {
		this.cliente = cliente;
	}
	public List<QuestionResponseTests> getQuestionsResponse() {
		return questionsResponse;
	}
	public void setQuestionsResponse(List<QuestionResponseTests> questionsResponse) {
		this.questionsResponse = questionsResponse;
	}
	public Date getFechaRegistro() {
		return fechaRegistro;
	}
	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
	
	
	
}
