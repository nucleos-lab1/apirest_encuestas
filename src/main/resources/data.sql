INSERT INTO QUESTION(ID_QUESTION, PREGUNTA, OPTION1, OPTION2, OPTION3) VALUES(1, '¿El producto cumplía con lo especificado en la web?', 'SI', 'NO', NULL);
INSERT INTO QUESTION(ID_QUESTION, PREGUNTA, OPTION1, OPTION2, OPTION3) VALUES(2, '¿ Cómo calificarías nuestro servicio general?', 'MALO', 'BUENO', 'EXCELENTE');
INSERT INTO QUESTION(ID_QUESTION, PREGUNTA, OPTION1, OPTION2, OPTION3) VALUES(3, '¿Nos recomendarías a tus amigos o colegas?', 'SI', 'NO', NULL);
INSERT INTO QUESTION(ID_QUESTION, PREGUNTA, OPTION1, OPTION2, OPTION3) VALUES(4, '¿Tienes algún comentario específico sobre cómo podemos satisfacer mejor tus necesidades?', null, null, NULL);

INSERT INTO CLIENTE(ID_CLIENTE, IDENTIFICACION, NOMBRES) VALUES(1, '0001', 'USUARIO GENERICO');