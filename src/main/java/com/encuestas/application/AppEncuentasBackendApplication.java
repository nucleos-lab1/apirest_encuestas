package com.encuestas.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppEncuentasBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppEncuentasBackendApplication.class, args);
	}

}
