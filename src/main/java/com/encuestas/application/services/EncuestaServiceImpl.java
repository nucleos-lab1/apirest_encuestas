package com.encuestas.application.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.encuestas.application.dao.IEncuestaDao;
import com.encuestas.application.dao.IQuestionDao;
import com.encuestas.application.models.Encuesta;
import com.encuestas.application.models.Question;
import com.encuestas.application.models.QuestionResponse;

@Service
public class EncuestaServiceImpl implements IEncuestaService{

	@Autowired
	private IQuestionDao questionDao;
	
	@Autowired
	private IEncuestaDao encuestaDao;
	
	@Override
	public List<Question> listarEncuesta() {
		return questionDao.findAll();
	}

	@Override
	public Encuesta save(Encuesta encuesta) {
		return  encuestaDao.save(encuesta);
	}

	@Override
	public Question findByQuestion(Long id) {
		return questionDao.findById(id).orElse(null);
	}

	@Override
	public Encuesta findByIdEncuesta(Long id) {
		return encuestaDao.findById(id).orElse(null);
	}

	@Override
	public List<Question> validacionPreguntasEncuesta(Encuesta encuesta) {
		
		List<Question> responseErr = new ArrayList<>();
		
		for (QuestionResponse questionResponse : encuesta.getQuestionsResponse()) {
			Question question = questionDao.getById(questionResponse.getQuestion().getIdQuestion());
			
			if(question != null) {
				//Verificacion Pregunta Abierta
				if(question.getOption1() == null && question.getOption2()== null && question.getOption3() == null) {
					if(questionResponse.getRespuesta() == null || questionResponse.getRespuesta().isEmpty()) {
						responseErr.add(question);
					}
				}else{
					if(!questionResponse.getRespuesta().equals(question.getOption1()) && !questionResponse.getRespuesta().equals(question.getOption2()) 
							&& !questionResponse.getRespuesta().equals(question.getOption3())) {
						responseErr.add(question);
					}
				}
			}else {
				//No existe
				Question questionErr = new Question();
				questionErr.setIdQuestion(0L);
				responseErr.add(questionErr);
			}
		}
		
		return responseErr;
	}

}
