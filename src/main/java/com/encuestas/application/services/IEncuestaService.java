package com.encuestas.application.services;

import java.util.List;

import com.encuestas.application.models.Encuesta;
import com.encuestas.application.models.Question;

public interface IEncuestaService {

	public List<Question> listarEncuesta();
	
	public Encuesta save(Encuesta encuesta);
	
	public Question findByQuestion(Long id);
	
	public Encuesta findByIdEncuesta(Long id);
	
	public List<Question> validacionPreguntasEncuesta(Encuesta encuesta);
}
