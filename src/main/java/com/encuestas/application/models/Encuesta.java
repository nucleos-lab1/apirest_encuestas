package com.encuestas.application.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Validated 
public class Encuesta implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEncuesta;
	
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@ManyToOne(fetch = FetchType.EAGER)
	@NotNull(message = "El cliente es Requerido")
	private Cliente cliente;
	
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@NotEmpty(message = "Las preguntas son Requeridas")
    private List<QuestionResponse> questionsResponse;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaRegistro;
	
	public Encuesta() {
		questionsResponse = new ArrayList<>();
	}
	
	@PrePersist
	public void preGuardarFecha() {
		this.fechaRegistro = new Date();
	}

	public Long getIdEncuesta() {
		return idEncuesta;
	}

	public void setIdEncuesta(Long idEncuesta) {
		this.idEncuesta = idEncuesta;
	}


	public List<QuestionResponse> getQuestionsResponse() {
		return questionsResponse;
	}

	public void setQuestionsResponse(List<QuestionResponse> questionsResponse) {
		this.questionsResponse = questionsResponse;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	

	
	
	

}
