package com.encuestas.application.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.encuestas.application.models.Encuesta;
import com.encuestas.application.models.Question;
import com.encuestas.application.services.IEncuestaService;

@Controller
@RequestMapping("/encuesta")
public class EncuestasController {
	
	@Autowired
	private IEncuestaService encuestaService;

	@GetMapping
	@Transactional(readOnly = true)
	public ResponseEntity<List<Question>> listarQuestion(){
		
		ResponseEntity<List<Question>> responseEntity = null;
		
		List<Question> questions = encuestaService.listarEncuesta();
		
		if(!questions.isEmpty()) {
			responseEntity = new ResponseEntity<List<Question>>(questions, HttpStatus.OK);
		}else {
			responseEntity = new ResponseEntity<List<Question>>(HttpStatus.NO_CONTENT);
		}
		
		return responseEntity;
	}
	
	@PostMapping
	public ResponseEntity<Map<String, Object>> createEncuesta(@Valid @RequestBody Encuesta encuesta, BindingResult result){
		
		ResponseEntity<Map<String, Object>> response =  null;
		Map<String, Object> responseMap = new HashMap<>();
		List<String> errors = null;
		List<Question> validacionErroresPreguntas = null;
		
		if(result.hasErrors()) {
			errors = new ArrayList<>();
			for (ObjectError error : result.getAllErrors()) {
				errors.add(error.getDefaultMessage());
			}
		}
		
		if(errors != null && !errors.isEmpty()) {
			responseMap.put("errors", errors);
			response = new ResponseEntity<Map<String,Object>>(responseMap, HttpStatus.BAD_REQUEST);
		}else {
			
			validacionErroresPreguntas = new ArrayList<Question>();
			validacionErroresPreguntas = encuestaService.validacionPreguntasEncuesta(encuesta);
			
			if(validacionErroresPreguntas.isEmpty()) {
				Encuesta enc = encuestaService.save(encuesta);
				
				if(enc != null) {
					responseMap.put("encuesta", enc);
					responseMap.put("mensaje", "Se Guardaron las preguntas Muchas Gracias por sus Respuestas!");
					response = new ResponseEntity<Map<String,Object>>(responseMap, HttpStatus.OK);
				}else {
					responseMap.put("mensaje", "Ocurrio un error al guardar la encuesta!");
					response = new ResponseEntity<Map<String,Object>>(responseMap, HttpStatus.NO_CONTENT);
				}
			}else {
				responseMap.put("mensaje", "Ocurrio un error al guardar la encuesta!");
				responseMap.put("preguntasConErrores", validacionErroresPreguntas);
				response = new ResponseEntity<Map<String,Object>>(responseMap, HttpStatus.BAD_REQUEST);
			}
			
			
		}
		return response;
	}
	
	@GetMapping("/{id}")
	@Transactional(readOnly = true)
	public ResponseEntity<Encuesta> findEncuesta(@PathVariable Long id){
		ResponseEntity<Encuesta> response =  null;
		
		Encuesta encuesta = encuestaService.findByIdEncuesta(id);
		
		if(encuesta != null) {
			response = new ResponseEntity<Encuesta>(encuesta, HttpStatus.OK);
		}else {
			response = new ResponseEntity<Encuesta>(HttpStatus.NO_CONTENT);
		}
		
		return response;
	}
	
}
