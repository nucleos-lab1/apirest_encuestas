package com.encuestas.application.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.encuestas.application.models.Question;

public interface IQuestionDao extends JpaRepository<Question, Long>{

}
