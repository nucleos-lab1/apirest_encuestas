package com.encuestas.application.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.encuestas.application.models.Encuesta;

public interface IEncuestaDao extends JpaRepository<Encuesta, Long>{

}
