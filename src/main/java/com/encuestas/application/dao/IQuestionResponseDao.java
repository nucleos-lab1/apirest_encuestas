package com.encuestas.application.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.encuestas.application.models.QuestionResponse;

public interface IQuestionResponseDao extends JpaRepository<QuestionResponse, Long>{

}
