package com.encuestas.application.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.encuestas.application.models.Cliente;

public interface IClienteDao extends JpaRepository<Cliente, Long>{

}
